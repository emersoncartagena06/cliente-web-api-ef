﻿using Ejemplo_Cliente_MVC_Web_API.Models;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using RestSharp;

namespace Ejemplo_Cliente_MVC_Web_API.Controllers
{
    public class ClienteController : Controller
    {
        public async Task<IActionResult> IndexAsync()
        {

            List<Cliente> lista = new List<Cliente>();

            var cliente = new RestClient("https://localhost:7256/API");
            var request = new RestRequest("Cliente", Method.Get);
            var response = await cliente.ExecuteAsync(request);

            if (response.IsSuccessful)
            {

                if(response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    lista = JsonConvert.DeserializeObject<List<Cliente>>(response.Content);
                }
                else
                {
                    ViewBag.mensaje = response.Content;
                }

            }
            else
            {
                ViewBag.mensaje = "No se pudo conectar al API";
            }

            return View(lista);
        }


        public async Task<IActionResult> Create()
        {
            return View(new Cliente());
        }

        [HttpPost]
        public async Task<IActionResult> Create(Cliente model)
        {
            if (ModelState.IsValid)
            {

                var cliente = new RestClient("https://localhost:7256/API");
                var request = new RestRequest("Cliente", Method.Post);

                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(model);

                var response = await cliente.ExecuteAsync(request);

                if (response.IsSuccessful)
                {

                    if(response.StatusCode == System.Net.HttpStatusCode.Created)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.mensaje = response.Content;
                    }

                }
                else
                {
                    ViewBag.mensaje = "No se pudo conectar al API";
                }

            }
            
            return View(model);
            
        }

        public async Task<IActionResult> Edit(int id)
        {

            var cliente = new RestClient("https://localhost:7256/API");
            var request = new RestRequest("Cliente/GetById/" + id.ToString(), Method.Get);
            var response = await cliente.ExecuteAsync(request);

            if (response.IsSuccessful)
            {
                if(response.StatusCode == System.Net.HttpStatusCode.OK)
                {
                    Cliente c1 = JsonConvert.DeserializeObject<Cliente>(response.Content);
                    return View(c1);
                }
                    
            }

            return RedirectToAction("Index");

            return View(new Cliente());
        }


        [HttpPost]
        public async Task<IActionResult> Edit(Cliente model)
        {
            if (ModelState.IsValid)
            {

                var cliente = new RestClient("https://localhost:7256/API");
                var request = new RestRequest("Cliente", Method.Put);

                request.AddHeader("content-type", "application/json");
                request.AddJsonBody(model);

                var response = await cliente.ExecuteAsync(request);

                if (response.IsSuccessful)
                {

                    if (response.StatusCode == System.Net.HttpStatusCode.OK)
                    {
                        return RedirectToAction("Index");
                    }
                    else
                    {
                        ViewBag.mensaje = response.Content;
                    }

                }
                else
                {
                    ViewBag.mensaje = "No se pudo conectar al API";
                }

            }

            return View(model);

        }
    }
}
